package ru.stc;

import java.sql.*;
import java.util.Arrays;
import java.util.Random;

public class JDBCDemo {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // шаг два, регистрация драйвера
        Class.forName("org.postgresql.Driver");

        // шаг три, инициализация переменных
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String pass = "postgres";

        // шаг четыре, подключение
        try(Connection connection = DriverManager.getConnection(
                url,
                user,
                pass
        )) {
            // создание таблицы
            createTable(connection);

            // запись в таблицу
            int randomId = new Random().nextInt();
            doInsert(connection, randomId);

            // чтение из таблицы
            printData(connection);
        }

        System.out.println("done!");
    }

    /**
     * создание таблиц
     *
     * @param connection
     * @throws SQLException
     */
    private static void createTable(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.execute("CREATE TABLE IF NOT EXISTS test(id INT PRIMARY KEY);");
            // "SELECT * FROM STREET_DICTIONARY WHERE CITY=?"
        }
    }

    /**
     * вставка в таблицу
     *
     * @param connection
     * @param randomId
     * @throws SQLException
     */
    private static void doInsert(Connection connection, int randomId) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO test VALUES (?)")) {
            statement.setInt(1, randomId);
            statement.executeUpdate();
        }
    }

    /**
     * чтение данных из бд и печать на экране
     *
     * @param connection
     */
    private static void printData(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM test")){

            while(resultSet.next()) {
                System.out.println(resultSet.getInt("id"));
            }

            statement.addBatch("CREATE TABLE IF NOT EXISTS t5(t INT PRIMARY KEY )");
            statement.addBatch("CREATE TABLE IF NOT EXISTS t6(t INT PRIMARY KEY )");
            int[] ints = statement.executeBatch();
            System.out.println("-------------------");
            Arrays.stream(ints).forEach(value -> System.out.println(value));
        }
    }

}
